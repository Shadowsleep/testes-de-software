﻿/*
 * Fixture é usando para criar colecoes compartilhadas de dados,
 * vc deve usar fixture quando vc quer centralizar a criação dos objetos para teste
 * [CollectionDefinition(nameof(ClienteCollection))] = cria a lista para compartilhamento
 *      e nas classes de teste onde vc ira testar os estados e comportamentos use  [Collection(nameof(ClienteCollection))]
 * e a partir desses da CollectionDefinition  e Collection vc pode usar injeção de dependencia e vc consegue obter seus dados
 * da classe fixture
 */

using System;
using Features.Clientes;
using Xunit;

namespace Features.Tests
{
    [CollectionDefinition(nameof(ClienteCollection))]
    public class ClienteCollection : ICollectionFixture<ClienteTestsFixture>
    {}

    public class ClienteTestsFixture : IDisposable
    {
        public Cliente GerarClienteValido()
        {
            var cliente = new Cliente(
                Guid.NewGuid(),
                "Eduardo",
                "Pires",
                DateTime.Now.AddYears(-30),
                "edu@edu.com",
                true,
                DateTime.Now);

            return cliente;
        }

        public Cliente GerarClienteInValido()
        {
            var cliente = new Cliente(
                Guid.NewGuid(),
                "",
                "",
                DateTime.Now,
                "edu2edu.com",
                true,
                DateTime.Now);

            return cliente;
        }

        public void Dispose()
        {
        }
    }
}